/**
 * Created by benji on 20/11/2017.
 */

let io = require('socket.io-client');
let axios = require('axios');
const Tools = require('../services/Tool.js');


export default class Comm {
    constructor() {
        this.comm = {};
        this.comm.io = {};
        this.socket = null;
        this.emitOnConnect = this.emitOnConnect.bind(this);

    }

    toString() {
        return '';
    }

    loadPres(presId, callback, callbackErr) {
        console.log("Load prez");
        axios.get('/loadPres')
            .then(function (data) {
                let size = Object.keys(data.data).length;
                let loadedPres = ""
                if (size > 0) {
                    loadedPres = data.data[Object.keys(data.data)[0]];
                }
                callback(loadedPres);
            })
            .catch(function (error) {
                callbackErr(error);
            });
    }

    loadContent(callback, callbackErr) {
        axios.get('/contents')
            .then(function (data) {
                callback(data.data);
            })
            .catch(function (error) {
                callbackErr(error);
            });

    }

    savPres(presJson, callback, callbackErr) {
        axios.post('/savePres', presJson)
            .then(function (response) {
                callback(response);
            })
            .catch(function (error) {
                callbackErr(error);
            });
    }

    savContent(contentJson, callback, callbackErr) {
        axios.post('/contents', contentJson)
            .then(function (response) {
                callback(response);
            })
            .catch(function (error) {
                callbackErr(error);
            });
    }

    fileUpload(fileC, callback, callbackErr) {
        let data = new FormData();
        data.append('file', fileC);
        axios.post('/file-upload', data)
            .then(function (response) {
                console.log(response);
                callback();
            })
            .catch(function (error) {
                callbackErr(error);
            });

    }

    emitOnConnect(message, presId) {
        console.log("message");
        console.log("socket");
        console.log(this.socket);
        console.log("this.comm.io.uuid");
        console.log(this.comm.io.uuid);
        this.socket.on('data_comm',
            test => this.play(presId)
        );
        this.socket.emit('data_comm', {'id': this.comm.io.uuid});
    }

    socketConnection(presId) {
        this.socket = io();

        // this.socket = io.connect(process.env.SOCKET_URL);
        // console.log("Socket url " + process.env.SOCKET_URL);
        this.comm.io.uuid = Tools.generateUUID();
        this.socket.on('connection', message => {
            this.emitOnConnect(message, presId)
        });

        // this.socket.on('newPres', function (socket) {
        //
        // });
        // this.socket.on('slidEvent', function (socket) {
        //
        // });
    }

    backward() {
        this.socket.emit('slidEvent', {'CMD': "PREV"});
    }

    forward() {
        this.socket.emit('slidEvent', {'CMD': "NEXT"});
    }

    play(presUUID) {
        this.socket.emit('slidEvent', {'CMD': "START", 'PRES_ID': presUUID});
    }

    pause() {
        this.socket.emit('slidEvent', {'CMD': "PAUSE"});
    }

    begin() {
        this.socket.emit('slidEvent', {'CMD': "BEGIN"});
    }

    end() {
        this.socket.emit('slidEvent', {'CMD': "END"});
    }

}



