/**
 * Created by benji on 21/11/2017.
 */
import React from "react";
import {FlatButton, IconButton} from "material-ui";
import {buttonCmd} from "../../../actions/index";
import {connect} from "react-redux";

class PresentationNavigation extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            presStarted: false
        };

        this.buttonClick = this.buttonClick.bind(this);

    }

    buttonClick(cmd) {
        this.props.dispatch(buttonCmd(cmd));
        if(cmd === 'PLAY'){
            this.setState({
                presStarted: true
            });
        }
    };

    render() {
        let content;
        if (this.state.presStarted) {
            content = <div>
                <IconButton tooltip="First" type="button"
                            onClick={() => this.buttonClick('FIRST')}>
                    <i className="material-icons">first_page</i>
                </IconButton>
                <IconButton tooltip="Previous" type="button"
                            onClick={() => this.buttonClick('PREVIOUS')}>
                    <i className="material-icons">fast_rewind</i>
                </IconButton>
                {/*<IconButton tooltip="Play" type="button"*/}
                {/*onClick={() => this.buttonClick('PLAY')}>*/}
                {/*<i className="material-icons">play_arrow</i>*/}
                {/*</IconButton>*/}
                {/*<IconButton tooltip="Pause" type="button"*/}
                {/*onClick={() => this.buttonClick('PAUSE')}>*/}
                {/*<i className="material-icons">pause</i>*/}
                {/*</IconButton>*/}
                <IconButton tooltip="Next" type="button"
                            onClick={() => this.buttonClick('NEXT')}>
                    <i className="material-icons">fast_forward</i>
                </IconButton>
                <IconButton tooltip="Last" type="button"
                            onClick={() => this.buttonClick('LAST')}>
                    <i className="material-icons">last_page</i>
                </IconButton></div>;
        }
        else {
            content = <div>
                <FlatButton label="Start presentation" primary={true}
                            onClick={() => this.buttonClick('PLAY')}/>
            </div>;
        }

        return (
            <div>
                {content}
            </div>
        )
    }
}

export default connect()(PresentationNavigation);