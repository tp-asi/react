/**
 * Created by benji on 25/10/2017.
 */
import React, {Component} from "react";
import Slid from "../../common/slid/containers/Slid";
import {connect} from "react-redux";
import {setSelectedSlid, updateSlid} from "../../../actions/index";
import PresentationNavigation from "./PresentationNavigation";
class EditSlidPanel extends Component {

    handleUpdateSlid(slid) {
        // this.setState(slid);
        this.props.dispatch(setSelectedSlid(slid));
        this.props.dispatch(updateSlid(slid));
    }

    render() {

        let slid;
        if (this.props.selected_slid !== null && this.props.selected_slid.id) {
            // slid = <p>{this.props.selected_slid.title}</p> ;
            slid = <Slid id={this.props.selected_slid.id} title={this.props.selected_slid.title}
                         txt={this.props.selected_slid.txt}
                         content_id={this.props.selected_slid.content_id}
                         displayMode="FULL_MNG"
                         handleUpdateSlid={this.handleUpdateSlid.bind(this)}/>;
        }
        else {
            slid = <p>Aucun slid selectionné</p>
        }
        return (
            <div>
                <div>
                    <PresentationNavigation />
                </div>
                <div>
                    {slid}
                </div>
            </div>
        );
    }
}
const mapStateToProps = (state, ownProps) => {
    return {
        selected_slid: state.selectedReducer.slid,
    }
};


export default connect(mapStateToProps)(EditSlidPanel);
