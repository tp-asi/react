/**
 * Created by benji on 20/11/2017.
 */
import React from "react";
import {FlatButton} from "material-ui";
import {connect} from "react-redux";
import {addSlid, removeSlid, saveCmd, setSelectedSlid} from "../../../actions/index";
const Tools = require('../../../services/Tool.js');


class CommandButtons extends React.Component {

    handleAdd = () =>{
        let slidArray = this.props.presentation.slidArray;
        let id;
        if(!slidArray || slidArray.length === 0){
            id = 1;
        }
        else{
            id = Tools.getNextSlidIndex(slidArray, slidArray[slidArray.length - 1].id);
        }
        let newSlid ={
            id : id,
            title : "",
            txt : "",
            content_id : null
        };
        this.props.dispatch(addSlid(newSlid));
        this.props.dispatch(setSelectedSlid(newSlid));
    };

    handleRemove = () => {
        if(this.props.selected_slid){
            this.props.dispatch(removeSlid(this.props.selected_slid));
            this.props.dispatch(setSelectedSlid(null));
        }
    };

    handleSave = () => {
        this.props.dispatch(saveCmd());
    };

    render() {
        return (
            <div>
                <FlatButton label="Add" primary={true} onClick={this.handleAdd} />
                <FlatButton label="Remove" secondary={true} onClick={this.handleRemove} />
                <FlatButton label="Save" onClick={this.handleSave} />
            </div>
        );
    }
}
const mapStateToProps = (state, ownProps) => {
    return {
        selected_slid: state.selectedReducer.slid,
        presentation: state.updateModelReducer.presentation,
    }
};
export default  connect(mapStateToProps)(CommandButtons);