/**
 * Created by benji on 25/10/2017.
 */

//Composant contenant le composant Presentation
import React from "react";
import Presentation from "../../common/presentation/containers/Presentation";
import CommandButtons from "./CommandButtons";

export default class BrowsePresentationPanel extends React.Component {
    constructor(props) {
        super(props);

    }

    render() {
        return (
            <div>
                <CommandButtons />
                <Presentation/>
            </div>
        );
    }
}
