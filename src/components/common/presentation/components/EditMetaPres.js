/**
 * Created by benji on 25/10/2017.
 */
import React from 'react';


// Composant visuel pour l’affichage et l’édition des meta data de la présentation
export default class EditMetaPres extends React.Component {

    render() {
        return (
            <div className="form-group">
                <label htmlFor="currentSlideTitle">Title </label>
                <input
                    type="text"
                    className="form-control"
                    id="currentSlideTitle"
                    onChange={this.props.handleChangeTitle}
                    value={this.props.title}
                />
                <label htmlFor="currentSlideText">Text</label>
                <textarea
                    rows="5"
                    type="text"
                    className="form-control"
                    id="currentSlideText"
                    onChange={this.props.handleChangeText}
                    value={this.props.description}>
                </textarea>
            </div>
        );
    }
}