/**
 * Created by benji on 25/10/2017.
 */
//Composant représentant la présentation (meta data et list de slids)
import React from "react";
import Slid from "../../slid/containers/Slid";
import {connect} from "react-redux";
import EditMetaPres from "../components/EditMetaPres";

class Presentation extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            id: this.props.id,
            title: this.props.title,
            description: this.props.description,
            slidArray: this.props.slidArray,
        }
    }

    handleChangeText(e) {
        this.setState({
            description: e.target.value
        });
    }

    handleChangeTitle(e) {
        this.setState({
            title: e.target.value
        });
    }

    render() {
        let slideDetail = [];

        if (this.props.slidArray) {
            for (let slid of this.props.slidArray) {
                slideDetail.push(
                    <Slid id={slid.id} title={slid.title} txt={slid.txt}
                          content_id={slid.content_id}
                          displayMode="SHORT" key={slid.id}/>
                )
            }
        }
        return (
            <div>
                <EditMetaPres title={this.state.title} description={this.state.description}
                              handleChangeText={this.handleChangeText.bind(this)}
                              handleChangeTitle={this.handleChangeTitle.bind(this)}/>
                <div>
                    {slideDetail}
                </div>
            </div>
        );
    }
}
const mapStateToProps = (state, ownProps) => {
    return {
        id: state.updateModelReducer.presentation.id,
        title: state.updateModelReducer.presentation.title,
        description: state.updateModelReducer.presentation.description,
        slidArray: state.updateModelReducer.presentation.slidArray,
    }
};

// export default Presentation;
export default connect(mapStateToProps)(Presentation);
