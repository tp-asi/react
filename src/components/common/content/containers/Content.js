/**
 * Created by benji on 25/10/2017.
 */
import React from "react";
import {connect} from "react-redux";
import {updateDraggedElt} from "../../../../actions/index";

class Content extends React.Component {
    constructor(props) {
        super(props);

        // this.state = {
        //     id : this.props.id,
        //     src : this.props.src,
        //     type : this.props.type,
        //     title : this.props.title,
        //     onlyContent : this.props.onlyContent //boolean
        // }
        this.onDragStart = this.onDragStart.bind(this);

    }

    onDragStart(event) {
        console.log("Drag start");
        this.props.dispatch(updateDraggedElt(this.props.id));
    }

    render() {
        let contentHTML;
        switch (this.props.type) {
            case "video":
                contentHTML = <iframe className="full" title={this.props.id}
                                      src={this.props.src}/>;
                break;
            case "img_url" :
            case "img" :
                contentHTML = <img src={this.props.src} className="full"
                                   alt={this.props.id}/>;
                break;
            case "web":
                contentHTML = <a href={this.props.src}>{this.props.src}</a>;
                break;
            default :
                break;
        }

        let title;
        if (!this.props.onlyContent) {
            title = <h3>{this.props.id} {this.props.title}</h3>;
        }

        return (
            <div>
                <div draggable="true" onDragStart={this.onDragStart}>
                    {contentHTML}
                    {title}
                </div>
            </div>
        );
    }
}

export default connect()(Content);