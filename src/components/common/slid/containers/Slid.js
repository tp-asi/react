/**
 * Created by benji on 25/10/2017.
 */
//Composant représentant un Slid (meta data et contenu)
import React from "react";
import Content from "../../../common/content/containers/Content";
import EditMetaSlid from "../components/EditMetaSlid";
import {setSelectedSlid} from "../../../../actions/index";
import {connect} from "react-redux";

class Slid extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            id: this.props.id,
            title: this.props.title,
            txt: this.props.txt,
            content_id: this.props.content_id,
            displayMode: this.props.displayMode // SHORT ou FULL_MNG
        };

        this.updateSelectedSlid = this.updateSelectedSlid.bind(this);
        this.onDropEvent = this.onDropEvent.bind(this);
        this.allowDrop = this.allowDrop.bind(this);
    }

    onDropEvent(event) {
        if (this.props.displayMode === "FULL_MNG") {
            console.log("DROP");
            this.dispatchSlid(this.props.title, this.props.txt, this.props.content_dragged);
        }
    }

    allowDrop(event) {
        if (this.props.displayMode === "FULL_MNG") {
            event.preventDefault();
        }
    }

    handleChangeText(e) {
        this.dispatchSlid(this.props.title, e.target.value, this.props.content_id);
    }

    handleChangeTitle(e) {
        this.dispatchSlid(e.target.value, this.props.txt, this.props.content_id);
    }

    dispatchSlid(title, txt, content_id) {
        // this.props.dispatch(updateSlid(this.props.id, title, txt, this.props.content_id));
        this.props.handleUpdateSlid({
            id: this.props.id,
            title: title,
            txt: txt,
            content_id: content_id
        });
    }

    updateSelectedSlid() {
        if (this.props.displayMode !== "FULL_MNG") {
            const tmpSlid = {
                id: this.props.id,
                title: this.props.title,
                txt: this.props.txt,
                content_id: this.props.content_id
            };
            this.props.dispatch(setSelectedSlid(tmpSlid));
        }
    }

    render() {
        let content = null;
        let contentHTML = null;
        if(this.props.contentMap === null){
            contentHTML = <p>Content is loading...</p>
        }
        else if(this.props.content_id !== null ){
            console.log(this.props.content_id);
            console.log(this.props.contentMap);
            content = this.props.contentMap[this.props.content_id];
            console.log(content);
            if(content === undefined){
                contentHTML = <p>No content, use drag and drop to add a content</p>
            }
            else{
            contentHTML = <Content id={content.id} src={content.src} type={content.type}
                                   title={content.title} onlyContent={true}/> ;
            }
        }
        else{
            contentHTML = <p>No content, use drag and drop to add a content</p>
        }

        let editPanel;
        if (this.props.displayMode === "FULL_MNG") {
            editPanel = <EditMetaSlid title={this.props.title} txt={this.props.txt}
                                      handleChangeText={this.handleChangeText.bind(this)}
                                      handleChangeTitle={this.handleChangeTitle.bind(this)}
            />
        }

        return (
            <div onClick={this.updateSelectedSlid} onDrop={this.onDropEvent}
                 onDragOver={this.allowDrop} className="border">
                <h3>{this.props.title}</h3>
                <p>{this.props.txt}</p>
                {contentHTML}
                {editPanel}
            </div>
        );
    }
}
const mapStateToProps = (state, ownProps) => {
    return {
        contentMap: state.updateModelReducer.content_map,
        content_dragged: state.selectedReducer.content_dragged,
    }
};


export default connect(mapStateToProps)(Slid);