/**
 * Created by benji on 25/10/2017.
 */
import React from "react";
import BrowseContent from "../components/BrowseContent";
import {connect} from "react-redux";
import {Dialog, RaisedButton} from "material-ui";
import AddContentPanel from "../components/AddContentPanel";
class BrowseContentPanel extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            open: false,
        };
    }



    handleOpen = () => {
        this.setState({open: true});
    };

    handleClose = () => {
        this.setState({open: false});
    };

    render() {
        let content = [];
        for (let key in this.props.contentMap) {
            if (!this.props.contentMap.hasOwnProperty(key))
                continue;
            let obj = this.props.contentMap[key];
            content.push(
                <BrowseContent content={obj} key={obj.id} />
            );
        }
        return (
            <div>
                {content}
                <RaisedButton label="Add" onClick={this.handleOpen}
                              className="full" primary={true}  />
                <Dialog
                    title="Add a new Content"
                    modal={false}
                    open={this.state.open}
                    onRequestClose={this.handleClose}>
                    <AddContentPanel onRequestClose={this.handleClose}/>
                </Dialog>
            </div>
        );
    }
}
const mapStateToProps = (state, ownProps) => {
    return {
        contentMap: state.updateModelReducer.content_map,
    }
};
export default connect(mapStateToProps)(BrowseContentPanel);
