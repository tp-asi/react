/**
 * Created by benji on 25/10/2017.
 */
import React from "react";
import Content from "../../common/content/containers/Content";

export default class BrowseContent extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            content: this.props.content,
        }
    }

    render() {
        let content = this.state.content;
        return (
            <div>
                <Content id={content.id} src={content.src} type={content.type}
                        title={content.title} onlyContent={false}/>
            </div>
        );
    }
}
