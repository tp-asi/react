/**
 * Created by benji on 20/11/2017.
 */
import React from "react";
import {FlatButton, MenuItem, SelectField} from "material-ui";
import {connect} from "react-redux";
import {addContent} from "../../../actions/index";
class AddContentPanel extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            title: "",
            type: "",
            src: "",
        };
    }

    handleTitleChange = (e) => {
        this.setState({
            title: e.target.value
        });
    };

    handleSrcChange = (e) => {
        this.setState({
            src: e.target.value
        });
    };

    handleTypeChange = (e, index, value) => {
        this.setState({
            type: value
        });
    };

    handleAddContent = () => {
        this.props.dispatch(addContent({
            title: this.state.title,
            type: this.state.type,
            src: this.state.src
        }));
        //close la dialog
        this.props.onRequestClose();
    };

    render() {
        let inputSrc;
        if (this.state.type === "img") {
            inputSrc =
                <div>
                    <input type="file" name="myImage" accept="image/*"
                           onChange={this.handleSrcChange}
                           value={this.state.src}/>
                </div>
        }
        else {
            inputSrc = <div><input
                type="text"
                className="form-control"
                id="currentValue"
                onChange={this.handleSrcChange}
                value={this.state.src}
            /></div>
        }

        return (
            <div className="form-group">
                <label htmlFor="currentSlideTitle">Title </label>
                <input
                    type="text"
                    className="form-control"
                    id="currentSlideTitle"
                    onChange={this.handleTitleChange}
                    value={this.state.title}
                />
                <div>
                    <SelectField
                        floatingLabelText="Content Type"
                        value={this.state.type}
                        onChange={this.handleTypeChange}>
                        <MenuItem value={'video'} primaryText="Vidéo"/>
                        <MenuItem value={'img'} primaryText="Image"/>
                        <MenuItem value={'img_url'} primaryText="Image URL"/>
                        <MenuItem value={'web'} primaryText="Web"/>
                    </SelectField>
                </div>
                <label htmlFor="currentValue">Lien</label>

                {inputSrc}
                <FlatButton label="Add" primary={true} onClick={this.handleAddContent}/>
                <FlatButton label="Cancel" secondary={true} onClick={this.props.onRequestClose}/>
            </div>
        );
    }
}
export default connect()(AddContentPanel);
