/**
 * Created by benji on 25/10/2017.
 */
import React from "react";
import "./main.css";
import "../../lib/bootstrap.min.css";
import BrowseContentPanel from "../browseContentPanel/containers/BrowseContentPanel";
import * as contentMapTmp from "../../source/contentMap.json";
import * as contentPresTmp from "../../source/pres.json";
import BrowsePresentationPanel from "../browsePresentationPanel/containers/BrowsePresentationPanel";
import EditSlidPanel from "../editSlidPanel/container/EditSlidPanel";
import {Provider} from "react-redux";
import {createStore} from "redux";
import globalReducer from "../../reducers/index";
import {updateContentMap, updatePresentation} from "../../actions/index";
import Comm from "../../services/Comm";
import MuiThemeProvider from "material-ui/styles/MuiThemeProvider";
import {Snackbar} from "material-ui";


// var comm = require('Comm');
const store = createStore(globalReducer);

export default class Main extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            contentMap: contentMapTmp,
            contentPres: contentPresTmp,
            snackbar: {
                open: false,
                text: ""
            }
        };

        // store.dispatch(updateContentMap(this.state.contentMap));
        // store.dispatch(updatePresentation(this.state.contentPres));

        this.comm = new Comm();
        this.getPres();
        this.getContentMap();

        store.subscribe(() => {
            // this.setState(
            //     {presentation:store.getState().updateModelReducer.presentation});
            // this.setState(
            //     {contentMap:store.getState().updateModelReducer.content_map});
            if (store.getState().commandReducer.cmdPres !== null) {
                this.handleCmd(store.getState().commandReducer.cmdPres, store.getState().updateModelReducer.presentation)

            }
        });

    }

    handleCmd(cmd, pres) {
        switch (cmd) {
            case 'SAVE_CMD' :
                this.comm.savPres(
                    pres,
                    () => this.savePresSuccess(),
                    () => this.savePresError()
                );
                break;
            case 'FIRST' :
                this.comm.begin();
                break;
            case 'PREVIOUS' :
                this.comm.backward();
                break;
            case 'PLAY' :
                this.comm.socketConnection(pres.id);
                break;
            case 'PAUSE' :
                this.comm.pause();
                break;
            case 'NEXT' :
                this.comm.forward();
                break;
            case 'LAST' :
                this.comm.end();
                break;
            default :
                this.setState({
                    snackbar: {
                        open: true,
                        text: "Wrong command"
                    }
                });
        }
    }

    savePresSuccess() {
        this.setState({
            snackbar: {
                open: true,
                text: "Presentation saved"
            }
        });
    }

    savePresError() {
        this.setState({
            snackbar: {
                open: true,
                text: "Error while saving presentation"
            }
        });
    }

    getPres() {
        let id = "efa0a79a-2f20-4e97-b0b7-71f824bfe349";
        this.comm.loadPres(id,
            pres => this.getPresSuccess(pres),
            error => this.getPresError(error)
        );
    }

    getPresSuccess(pres) {
        store.dispatch(updatePresentation(pres));
    }

    getPresError(error) {
        this.setState({
            snackbar: {
                open: true,
                text: "Error while loading presentation"
            }
        });
    }

    getContentMap() {
        this.comm.loadContent(
            contentMap => this.getContentMapSuccess(contentMap),
            error => this.getContentMapError(error)
        );
    }

    getContentMapSuccess(contentMap) {
        store.dispatch(updateContentMap(contentMap));
    }

    getContentMapError(error) {
        this.setState({
            snackbar: {
                open: true,
                text: "Error while loading content map"
            }
        });
    }


    render() {
        return (
            <MuiThemeProvider>
                <Provider store={store}>
                    <div className='container-fluid height-100'>
                        <div className="row height-100">
                            <div className='col-md-3 col-lg-3 height-100 vertical-scroll'>
                                <BrowsePresentationPanel />
                            </div>
                            <div className='col-md-6 col-lg-6 height-100'>
                                <EditSlidPanel/>
                            </div>
                            <div className='col-md-3 col-lg-3 height-100'>
                                <BrowseContentPanel/>
                            </div>
                        </div>
                        <Snackbar
                            open={this.state.snackbar.open}
                            message={this.state.snackbar.text}
                            autoHideDuration={3000}
                        />
                    </div>

                </Provider>
            </MuiThemeProvider>
        );
    }
}
