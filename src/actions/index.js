/**
 * Created by benji on 25/10/2017.
 */


//Liste des actions permettant d’interagir avec le store
export const setSelectedSlid=(slid_obj)=>{
    return {
        type: 'UPDATE_SELECTED_SLID',
        obj:slid_obj
    };
};

export const updateContentMap=(content_map)=>{
    return {
        type: 'UPDATE_CONTENT_MAP',
        obj:content_map
    };
};

export const updatePresentation=(presentation)=>{
    return {
        type: 'UPDATE_PRESENTATION',
        obj:presentation
    };
};

export const updateSlid=(slid)=>{
    return {
        type: 'UPDATE_PRESENTATION_SLIDS',
        obj:slid
    };
};

export const updateDraggedElt=(id)=>{
    return {
        type: 'UPDATE_DRAGGED_ELT',
        obj:id
    };
};

export const addContent=(content)=>{
    return {
        type: 'ADD_CONTENT',
        obj: content
    };
};

export const addSlid=(slid)=>{
    return {
        type: 'ADD_PRESENTATION_SLID',
        obj: slid
    };
};

export const removeSlid=(slid)=>{
    return {
        type: 'DELETE_PRESENTATION_SLID',
        obj: slid
    };
};

export const saveCmd=()=>{
    return {
        type: 'SAVE_CMD',
        obj: null
    };
};

export const buttonCmd=(cmd)=>{
    return {
        type: 'BUTTON_CMD',
        obj: cmd
    };
};