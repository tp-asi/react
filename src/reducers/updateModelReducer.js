/**
 * Created by benji on 25/10/2017.
 */


//Reducer mettant à jour les différents modèles utilisés contenMap et current_pres
const Tools = require('../services/Tool.js');
const updateModelReducer = (state = {presentation: {}, content_map: {}}, action) => {
    console.log(action);
    let slid;
    let slidArray;
    let presentation;
    switch (action.type) {
        case 'UPDATE_PRESENTATION':
            return {presentation: action.obj, content_map: state.content_map};
        case 'UPDATE_PRESENTATION_SLIDS':
            slid = action.obj;
            slidArray = state.presentation.slidArray.slice();
            for (let i = 0; i < slidArray.length; i++) {
                if (slidArray[i].id === slid.id) {
                    slidArray[i] = {
                        id: slid.id,
                        title: slid.title,
                        txt: slid.txt,
                        content_id: slid.content_id
                    };
                    break;
                }
            }
            presentation = {
                id: state.presentation.id,
                title: state.presentation.title,
                description: state.presentation.description,
                slidArray: slidArray
            };
            return {presentation: presentation, content_map: state.content_map};
        case 'ADD_PRESENTATION_SLID':
            slid = action.obj;
            slidArray = state.presentation.slidArray.slice();

            slidArray.push(slid);

            presentation = {
                id: state.presentation.id,
                title: state.presentation.title,
                description: state.presentation.description,
                slidArray: slidArray
            };
            return {presentation: presentation, content_map: state.content_map};
        case 'DELETE_PRESENTATION_SLID':
            slid = action.obj;
            slidArray = state.presentation.slidArray.slice();
            for (let i = 0; i < slidArray.length; i++) {
                if (slidArray[i].id === slid.id) {
                    slidArray.splice(i,1);
                    break;
                }
            }
            presentation = {
                id: state.presentation.id,
                title: state.presentation.title,
                description: state.presentation.description,
                slidArray: slidArray
            };
            return {presentation: presentation, content_map: state.content_map};
        case 'UPDATE_CONTENT_MAP':
            return {presentation: state.presentation, content_map: action.obj};
        case 'ADD_CONTENT':
            let content_map = state.content_map;
            let id = Tools.generateUUID();
            content_map[id] = Object.assign({id : id}, action.obj);
            return {presentation: state.presentation, content_map: content_map};
            return; //TO DO
        default:
            return state;
    }
};
export default updateModelReducer;
