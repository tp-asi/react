/**
 * Created by benji on 21/11/2017.
 */
const commandReducer = (state = {cmdPres: null}, action) => {
    switch (action.type) {
        case 'SAVE_CMD':
            return {cmdPres: 'SAVE_CMD'};
        case 'BUTTON_CMD':
            return {cmdPres: action.obj};
        default:
            return {cmdPres: null};
    }
};
export default commandReducer;
